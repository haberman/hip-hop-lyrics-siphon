#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import csv
import json
import logging
import argparse
import operator
import lyricsgenius
import private

PROG_NAME = 'Hip Hop Lyrics Siphon'

ITER_LIMIT = 200
ITER_START = 1400
ITER_REQ = 20


def cache_search(key, value, cache):
    return [artist for artist in cache if artist[key] == value]


def filter_csv(csv_path, verbose, must_be_verified,
               min_followers, min_songs, cache=None):

    genius = lyricsgenius.Genius(private.GENIUS_ACCESS_TOKEN)
    genius.verbose = verbose

    with open(csv_path) as csv_file:

        csv_reader = csv.DictReader(csv_file)

        valid_artists = []
        line_count = 0

        if verbose:
            logging.info(f'Parsing {csv_path}')

        for row in csv_reader:
            if line_count == 0 or line_count < ITER_START:
                line_count += 1
                continue
            elif line_count > ITER_START + ITER_LIMIT:
                break

            if verbose:
                print(f'\n-------------------\nSearching {row["artist"]}...')

            artist_search = genius.search_genius(row['artist'], ITER_REQ*2)
            real_hits = 0
            unique_artists = {}
            line_count += 1

            if 'hits' not in artist_search:
                continue

            for hit in artist_search['hits']:
                artist = hit['result']['primary_artist']
                artist_path = artist['api_path']

                if artist_path not in unique_artists:
                    unique_artists[artist_path] = {'id': artist['id'], 'count': 1}
                else:
                    unique_artists[artist_path]['count'] += 1

                real_hits += 1

            if len(unique_artists.keys()) == 0:
                if verbose:
                    logging.warning(f'Skipping {row["artist"]} because of non existence.')
                continue

            top_artist_key = sorted(unique_artists, key=operator.itemgetter(1))[0]
            top_artist = unique_artists[top_artist_key]

            # if (top_artist['count']/real_hits) < credit_ratio:
            #     if verbose:
            #         logging.warning(f'Skipping {row["artist"]} because of mentions\' ratio.')
            #         print(unique_artists)
            #     continue

            if cache:
                cached_artist = cache_search('id', top_artist['id'], cache)
                if len(cached_artist) > 0:
                    if verbose:
                        logging.info(f'Already cached.')
                    continue

            valid_artist = genius.get_artist(top_artist['id'])['artist']
            if not bool(valid_artist['is_verified']) and must_be_verified:
                if verbose:
                    logging.warning(f'Skipping artist because profile is not verified')
                continue

            if min_followers and valid_artist['followers_count'] < min_followers:
                if verbose:
                    logging.warning(f'Skipping artist because of followers\' count.')
                continue

            page = 1
            songs_count = 0
            while songs_count < min_songs:
                songs = genius.get_artist_songs(valid_artist['id'], 'title', ITER_REQ, page)['songs']
                for song in songs:
                    if song['lyrics_state'] != 'complete':
                        continue
                    songs_count += 1
                
                if page > 4:
                    break
                page += 1

            if songs_count < min_songs and verbose:
                logging.warning(f'Skipping artist ({row["artist"]}) because of songs\' count.')
                continue

            if verbose:
                logging.info('Valid!')

            # reducing saved file weight
            valid_artist.pop('user', None)
            valid_artist.pop('description_annotation', None)
            valid_artist.pop('current_user_metadata', None)
            valid_artists.append(valid_artist)

        if verbose:
            l_output = len(valid_artists)
            p_filtered = '0.00%'

            if l_output > 0:
                p_filtered = '{:.2%}'.format(l_output/(line_count-ITER_START))

            logging.info(f'Processed {len(valid_artists)} artists. ({p_filtered} of input)')
        if cache:
            return cache + valid_artists
        else:
            return valid_artists


def number_clamp(v_in, v_min, v_max):
    return max(min(v_in, v_max), v_min)


def parse_json(json_path):
    return None


if __name__ == '__main__':

    CSV_MUSICIANS = 'data/in/hip-hop_musicians.csv'
    JSON_MUSICIANS = 'data/out/hip-hop_musicians.json'

    MIN_MENTION_RATIO = .5
    MIN_FOLLOWERS = 20
    MIN_SONGS = 30

    logging.basicConfig(format='%(levelname)s:%(message)s',
                        level=logging.INFO)

    parser = argparse.ArgumentParser(prog='./{}.py'.format(PROG_NAME),
                                     description='API for NLP analytics and machine generated poetry.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Increase output verbosity.')
    parser.add_argument('-c', '--cache', default=JSON_MUSICIANS,
                        help='An input json file of already filtered artists.')

    subparsers = parser.add_subparsers(help='Required actions')

    filter_subparser = subparsers.add_parser('filter',
                                             help='Filters a raw csv musicians list with popularity based parameters.')

    filter_subparser.add_argument('input', type=str, nargs='?', default=CSV_MUSICIANS,
                                  help='a csv file reporting hip hop musicians with, at least, an `artist` column')
    filter_subparser.add_argument('-s', '--songs', type=int, default=MIN_SONGS,
                                  help='number of songs an artiste should have written.')
    filter_subparser.add_argument('-f', '--followers', type=int, default=None,
                                  help='number of Genius followers.')
    filter_subparser.add_argument('-v', '--verified', action='store_true',
                                  help='should the Genius account be verified?')

    # siphon_subparser = subparsers.add_parser('siphon',
    #                                          help='Get lyrics for given artists\' ids and resolved parameters.')

    try:
        args = parser.parse_args()
        cached_artists = None

        if not os.path.exists(args.cache) and args.verbose:
            logging.warning(f'Input cache file `{args.cache}` does not exist, creating...')
            with open(args.cache, 'w') as cached_file:
                cached_artists = {}
                json.dump(cached_artists, cached_file)
        elif os.path.splitext(args.cache)[1] != '.json' and args.verbose:
            logging.warning(f'Input `{args.cache}` appears not to be a json file.')
        else:
            with open(args.cache) as cached_file:
                cached_artists = json.load(cached_file)

        if cached_artists and args.verbose:
            logging.info(f'Found {len(cached_artists)} cached artists.')

        do_filter = 'songs' in args
        # do_siphon = 'siphon' in args

        if do_filter:
            if type(args.input) is list:
                args.input = args.input[0]

            if not os.path.exists(args.input):
                logging.warning(f'Input file `{args.input}` does not exist.')
                exit(0)

            ext = os.path.splitext(args.input)[1]
            if ext == '.csv':
                valid_artists = filter_csv(args.input, args.verbose, args.verified,
                                           args.followers, args.songs,
                                           cached_artists)

                with open(args.cache, 'w') as json_handler:
                    json.dump(valid_artists, json_handler)
            else:
                logging.warning(f'Input file format `{ext}` is not recognized.')

    except IOError as msg:
        logging.warn(f'Argparse error: {msg}')
